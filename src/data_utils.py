from sklearn.feature_extraction.text import CountVectorizer


vectorizer = None


def get_texts_X_and_words(texts):
    global vectorizer
    vectorizer = CountVectorizer(ngram_range=(1, 2), max_features=100000, min_df=0.01, max_df=0.95)
    texts_X = vectorizer.fit_transform(texts)
    labels = [None] * len(vectorizer.vocabulary_)
    for k, v in vectorizer.vocabulary_.items():
        labels[v] = k

    return (labels, texts_X.toarray())


def transform(text):
    if vectorizer is None:
        raise EnvironmentError("Can't transform text without fit")
    return vectorizer.transform(text)
