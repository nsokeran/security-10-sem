import re

import numpy as np

from src.nl_utils import stem_eng_word

classes_description = [
    [0, "sql injection"],
    [1, "xss"],
    [2, "buffer overflow"],
    [3, "android"],
    [4, "linux"],
    [5, "windows"],
    [6, "phishing"],
    [7, "shellcode"]
]


def get_names_by_class_ids(binary_ids):
    names = []
    for i, is_class in enumerate(binary_ids):
        if is_class:
            names.append(classes_description[i][1])
    return names


def get_sorted_top_words(labels_X, text_X, n_top=10):
    indices = np.argsort(text_X)[::-1]
    top_features = [labels_X[i] for i in indices[:n_top] if text_X[i] > 0.0]
    return top_features


def find_texts_with_words(labels_X, texts_X, words, n_top):
    text_numbers = []
    normalized_words = set(map(lambda x: stem_eng_word(x), words))
    for i, text_X in enumerate(texts_X):
        top_words = get_sorted_top_words(labels_X, text_X, n_top)
        if normalized_words.issubset(set(top_words)):
            text_numbers.append(i)
    return text_numbers


def to_binary_y(texts_X, numbers_y):
    max_id = 0
    for y in numbers_y:
        if len(y) > 0:
            max_id = max([max_id, max(y)])

    binary_y = [[0] * (max_id + 1) for i in range(texts_X.shape[0])]

    for i, y in enumerate(numbers_y):
        for class_id in y:
            binary_y[i][class_id] = 1

    return np.array(binary_y)


def get_classes(text_names):
    result = {}
    with open("meta/class_assignments.txt", encoding="utf-8") as f:
        assignments = f.readlines()

    for assignment in assignments:
        matcher = re.search("^(.*) \[(.*)\]$", assignment)
        text_name = matcher.group(1)
        classes = list(map(int, matcher.group(2).split()))
        result[text_name] = classes

    sorted_classes = []
    for text_name in text_names:
        if not result.get(text_name):
            raise EnvironmentError("No class assignments for text " + text_name)
        sorted_classes.append(result[text_name])
    return sorted_classes


def trim_absent_classes(X, y):
    new_X = []
    new_y = []
    for i, y_row in enumerate(y):
        if max(y_row) > 0:
            new_X.append(X[i, :])
            new_y.append(y_row)

    return np.array(new_X), np.array(new_y)
