import nltk
import numpy as np
from nltk.corpus import stopwords
from nltk.stem.snowball import EnglishStemmer
from nltk.tokenize import word_tokenize

nltk.download("stopwords")
nltk.download("punkt")
nltk.download("words")

english_stemmer = EnglishStemmer()
stop_words = stopwords.words('english')
symbols = "( ) ; : [ ] , . ! ? / \ < > | _ = + - @ ' \" ` * ^ % 0 1 2 3 4 5 6 7 8 9".split()


def stem_eng_word(word):
    return english_stemmer.stem(word)


def extract_stemmed_text(text):
    normalized_text = ''.join([char if char not in symbols else ' ' for char in text.lower()])
    words = word_tokenize(normalized_text)
    words = [word for word in words if word not in stop_words and len(word) > 1]
    words = map(stem_eng_word, words)
    return " ".join(words)


def stem_X(X):
    X_clone = np.array(X, copy=True)
    rows = X_clone.shape[0]
    for row in range(rows):
        X_clone[row] = extract_stemmed_text(X_clone[row])
    return X_clone
