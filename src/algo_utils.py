from sklearn.multiclass import OneVsRestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import GridSearchCV


def get_best_clf(clf, params, X_train, y_train):
    gs = GridSearchCV(clf, params, cv=5, scoring='f1_micro')
    gs.fit(X_train, y_train)
    return gs, gs.best_estimator_


def get_best_sgd(X, y):
    return get_best_clf(
        OneVsRestClassifier(SGDClassifier(max_iter=5)),
        {
            'estimator__alpha': [1e-4]
        }, X, y
    )
