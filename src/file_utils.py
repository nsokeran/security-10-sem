import os

from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import LAParams, LTTextBox, LTTextLine
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage

from src.nl_utils import extract_stemmed_text


def read_pdf_io(io):
    rsrcmgr = PDFResourceManager()
    laparams = LAParams()
    device = PDFPageAggregator(rsrcmgr, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, device)

    extracted_text = ""
    for page in PDFPage.get_pages(io):
        interpreter.process_page(page)
        layout = device.get_result()
        for lt_obj in layout:
            if isinstance(lt_obj, LTTextBox) or isinstance(lt_obj, LTTextLine):
                extracted_text += lt_obj.get_text()

    return extracted_text


def read_pdf(filename):
    with open(filename, "rb") as f:
        return read_pdf_io(f)


def read_all_converted_texts(base_dir):
    texts = []
    for file in sorted(os.listdir(base_dir)):
        if file.endswith(".txt"):
            with open(base_dir + "/" + file, "rb") as f:
                text = f.read().decode("utf-8")
                texts.append((file, extract_stemmed_text(text)))

    return texts
