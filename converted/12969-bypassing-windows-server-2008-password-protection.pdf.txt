 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Title: Bypassing  Windows  Server  2008 
Password Protection 
Author:  
Charalambous Glafkos 
Handle:  
nowayout 
Mail:   
glafkos@astalavista.com 
Website:  
http://www.astalavista.com 
Date: 
January 29th 2009 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
1
 
Table of Contents 
TABLE OF CONTENTS ............................................................................................................................................2 
INTRODUCTION .......................................................................................................................................................3 
BYPASSING WINDOWS SERVER 2008 PASSWORD PROTECTION..............................................................3 
TOOLS .........................................................................................................................................................................3 
DEMONSTRATION...................................................................................................................................................3 
CONCLUSION ............................................................................................................................................................7 
REFERENCE ..............................................................................................................................................................7 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
2
 
Introduction 
 
Windows Server 2008 is the most recent operating system release for Microsoft Windows Server line. It is 
based  on  Windows  NT  6.0  SP1  kernel,  like  Windows  Vista;  therefore  it  shares  much  of  the  same 
architecture and technology as Windows Vista including new futures such as security, management and 
administrative futures. 
Although  very  secured  compared  to  earlier  versions  it  can  be  bypassed  quite  easily  having  physical 
access to the machine.  
In  addition  to  security,  memory  protection  mechanisms  are  available  such  as  GS,  SafeSEH,  Heap 
protection, DEP and ASLR which are beyond the scope of this paper. 
Bypassing Windows Server 2008 Password Protection  
 
In this paper we will demonstrate a simple and effective way to bypass Windows Server 2008 password 
protection in the case where we have forgotten the password or it has been changed by a third party and 
we have to get access to our system. 
* Note: Do NOT use this approach to backdoor any server in your work environment 
Tools 
 
PING (Partimage Is Not Ghost)  
Download: http://ping.windowsdream.com/ping/Releases/3.00.01/PING-3.00.iso (~22MB) 
Demonstration 
 
To be able to bypass the password protection we will need first to boot with PING cd or any 
other  bootable  live  Linux  distribution  that  supports  NTFS-3G  driver  (NTFS-3G  is  a  cross-
platform  implementation  of  the  Windows    NTFS  file  system  that  supports  read/write 
capabilities). 
 
We  first  check  which  partition  is  the  Windows  NTFS  partition  which  in  our  example  is 
/dev/sda1.  
fdisk –l | grep NTFS 
 
Then we create the directory where we will mount the windows files : 
mkdir –p /mnt/windows 
 
Therefore, we mount with NTFS-3G driver the windows partition to /mnt/windows folder: 
mount –t ntfs-3g /dev/sda1/mnt/windows 
 
 
 
3
Now  we  do  the  trick  by  replacing  some  executable  files  with  cmd.exe  which  in  our 
demonstration would be the Magnify.exe tool that can be found on Password Protection screen 
under Ease of Access options. 
 
mv Magnify.exe Magnify.bck 
cp cmd.exe Magnify.exe 
 
 
 
After  we  do  the  changes  described  above  we  restart  the  machine  and  boot  back  to  Windows 
Server 2008. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
4
As  we  can  see  in  the  snapshot  below  under  Ease  of  Access  we  have  the  option  to  select  the 
Magnifier  tool  and  give  ourselves  a  command  prompt  with  NT  AUTHORITY/SYSTEM 
privileges on the system 
 
 
 
5
 
 
 
 
 
6
6
 
The following approach can be used with Windows Vista and also by any other Ease of Access 
tools or even by Ease of Access itself by renaming “utilman.exe” to “cmd.exe”. 
Conclusion 
 
I hope you may find the following paper useful and keep it for your reference in case you have 
forgotten your Windows Server 2008 / Vista password.  
 
The  above  paper  has  been  written  from  an  article  I  wrote  for  IT  Solutions  Knowledge  Base 
website which I think you might find usefull for your everyday IT Solutions. 
Reference 
 
http://www.itsolutionskb.com 
http://ping.windowsdream.com 
 
 
7
