Learn Information Gathering By Example
Aelphaeis Mangarae [IRC.EFnet.Org #d-u]
July 11th 2006
http://www.SecurZone.org
Join Aelphaeis Mangarae On IRC:
irc.EFnet.org #d-u
© Copyright Aelphaeis Mangarae 2006
www.SecurZone.Org                                       __________________  
Table Of Contents
Introduction
Utilizing Search Engines
Utilizing NetCraft
Using Whois
Utilizing NSLookup
Brute Forcing DNS Names
Performing A Ping Sweep
Identifying Firewalls/Routers
Gathering Information From Emails
Obtaining Information With Scripts
About The Author
Greetz To
www.SecurZone.Org                                       __________________  
Introduction
Information Gathering is usually the first done when Penetration testing.
It is indeed a very important part in Penetration testing, and no Penetration tester or Internet security 
enthusiast can be left with out the knowledge of not knowing how to successfully gather information 
on a target.
This white paper goes through the steps and tools you can use in order to successfully gather 
information on a target web server.
www.SecurZone.Org                                       __________________  
Utilizing Search Engines
Search Engines can be used (In particular Google) to gather information on a target host.
The first and probably most powerful search string in Google is the “site:” string.
This will give us all of the pages that have been indexed by Google, this can be used to gather sub 
domains.
Although this can usually be gathered through other means, using Google is an effective and 
anonymous way of getting this sort of information.
As you can see the second listing down gives us a Boeing sub domain “jobs.boeing.com”.
If you search “site:boeing.com” in Google and scroll down you will find more sub domains.
Some other interesting queries are:
link:targethost.com
"Company Name Here"
Email Addresses (Obtained from the website or Whois Database.)
Forum Usernames
You may be wondering why search Email Addresses and Forum Usernames? Well the concept is 
quite simple. It is very possible that the administrators talk on Forums, possibly to get help with their 
website. The posts they make on the Forums may contains information relating to the website. It is 
also possible (If the admin is incompetent enough) that they may use the same password for 
numerous things. Hence obtaining their forum password would be very useful. 
www.SecurZone.Org                                       __________________  
Utilizing NetCraft
NetCraft can be a very useful tool in Penetration testing, as it often delivers a lot of information on the 
target host.
NetCraft can give you information such as:
*A history of web servers used by the target host.
*A history of Operating Systems used by the target host.
*Approximate times of when web server software, Operating System or host have been changed.
*The owner of the target host.
*IP Address of the target host.
The information provided by NetCraft would be useful in knowing for many reasons.
Information on the web server used by the target host as well as the Operating System can be used 
to be able to work out what vulnerabilities the host would have.
If the target host seems to be moving servers a lot it may be because his website is mirrored on two 
or more servers.
www.SecurZone.Org                                       __________________  
Using Whois
A Whois is a target host can be done using many software applications and websites.
For this paper I am going to use some websites to do a Whois on a target host.
Using the Domain Dossier at Central Ops (http://centralops.net/co/) we able to gather a lot of 
important information on our target host.
Using Domain Dossier we can do a Whois on our target (Chasenet.org.)
If we want we can also do a basic service scan and a traceroute.
By using Domain Dossier we were able to get information on Domain (General Whois Information), 
Network Information as well as DNS records.
After doing a Whois you should look for things of importance, such as:
*Registrant Name
*Registrant Organization
*Registrant Street, City, Town, Country, Postal Code
*Registrant Phone and/or Fax number(s)
*Email Addresses (Useful for Social Engineering, along with other details)
*Name Servers (Useful for Zone Transfers)
*Mail Servers (Might be useful for faking/spoofing mail)
*IP Block/Range (This will be needed to conduct a Ping Sweep)
For people who wish to do a Whois on a domain that doesn’t seem be bring up any information (using 
Domain Dossier) you would want to use a region specific Whois database.
These would also be useful when doing a Reverse DNS.
Europe:
http://www.ripe.net/perl/whois/
Asia Pacific Region:
http://www.apnic.net/apnic-bin/whois.pl
Latin American and Caribbean:
http://www.lacnic.net/cgi-bin/lacnic/whois
www.SecurZone.Org                                       __________________  
African And Indian Ocean:
http://www.afrinic.net/cgi-bin/whois
North America:
http://www.arin.net/whois/
Military Whois:
http://www.nic.mil/dodnic/
Government Whois:
http://www.igotit.cc/usgov.html
www.SecurZone.Org                                       __________________  
Utilizing NSLookup
What Is NSLookup?
NSLookup is simply a tool that can be used to gather information on a target via a Zone Transfer.
This information can be useful when doing Information Gathering during a Penetration Test.
Before we begin I will explain some of the switches (options) that can be used with NSLookup.
-t Lists all records of the type specified. (In the Windows version of NSLookup it says this is an invalid 
option, so just use the other options by themselves.)
-a Lists all of the aliases for the hosts in the domain.
-h Lists CPU and Operating System information for the domain.
-s Lists well-known services of hosts in the domain.
-d Lists all records for the domain.
Let’s now do a Whois on a target.
Here is the information NSLookup gave me on the target host:
Note: Notice I had to give NSLookup a Name Server to use, you will have to do this too when 
using NSLookup (well I would recommend doing it anyway.)
------------
www.SecurZone.Org                                       __________________  
Got answer:
    HEADER:
        opcode = QUERY, id = 1, rcode = NOERROR
        header flags:  response, auth. answer, want recursion, recursion avail.
        questions = 1,  answers = 1,  authority records = 1,  additional = 1
    QUESTIONS:
        64.200.235.66.in-addr.arpa, type = PTR, class = IN
    ANSWERS:
    ->  64.200.235.66.in-addr.arpa
        name = rdns1.ipowerweb.net
        ttl = 3600 (1 hour)
    AUTHORITY RECORDS:
    ->  200.235.66.in-addr.arpa
        nameserver = rdns2.ipowerweb.net
        ttl = 3600 (1 hour)
    ADDITIONAL RECORDS:
    ->  rdns2.ipowerweb.net
        internet address = 66.235.216.64
        ttl = 1052 (17 mins 32 secs)
------------
Server:  rdns1.ipowerweb.net
Address:  66.235.200.64
------------
Got answer:
    HEADER:
        opcode = QUERY, id = 2, rcode = NOERROR
        header flags:  response, want recursion, recursion avail.
        questions = 1,  answers = 1,  authority records = 4,  additional = 4
    QUESTIONS:
        www.grandtheftcountry.com, type = A, class = IN
    ANSWERS:
    ->  www.grandtheftcountry.com
        internet address = 72.22.69.19
        ttl = 3523 (58 mins 43 secs)
    AUTHORITY RECORDS:
    ->  grandtheftcountry.com
        nameserver = ns1.ipowerweb.net
        ttl = 3523 (58 mins 43 secs)
    ->  grandtheftcountry.com
        nameserver = ns1.ipowerdns.com
        ttl = 3523 (58 mins 43 secs)
    ->  grandtheftcountry.com
        nameserver = ns2.ipowerweb.net
        ttl = 3523 (58 mins 43 secs)
    ->  grandtheftcountry.com
        nameserver = ns2.ipowerdns.com
        ttl = 3523 (58 mins 43 secs)
    ADDITIONAL RECORDS:
    ->  ns1.ipowerweb.net
        internet address = 64.70.61.130
        ttl = 136245 (1 day 13 hours 50 mins 45 secs)
    ->  ns1.ipowerdns.com
        internet address = 66.235.217.202
        ttl = 114042 (1 day 7 hours 40 mins 42 secs)
    ->  ns2.ipowerweb.net
        internet address = 66.235.217.200
        ttl = 136245 (1 day 13 hours 50 mins 45 secs)
    ->  ns2.ipowerdns.com
        internet address = 64.70.61.131
        ttl = 117643 (1 day 8 hours 40 mins 43 secs)
------------
Non-authoritative answer:
Name:    www.grandtheftcountry.com
Address:  72.22.69.19
www.SecurZone.Org                                       __________________  
If you don’t wish to use the UNIX or Windows command line NSLookup you do have the option of 
using a web based one (How good each of these are I don’t know.)
http://www.kloth.net/services/nslookup.php
http://www.zoneedit.com/lookup.html
http://www.webmaster-toolkit.com/ns-lookup.shtml
http://www.bankes.com/nslookup.htm
http://www.infobear.com/nslookup.shtml
The Linux equivalent of NSLookup is Dig.
Usage: dig @NameServer host.com
Result:
www.SecurZone.Org                                       __________________  
With these particular servers we haven’t really had any luck retrieving any sub domains. 
What information you get will vary from server to server, depending on how they are 
configured.
www.SecurZone.Org                                       __________________  
Brute Forcing DNS Names
We have tried some ways of obtaining sub domains for attacking. But how about brute forcing them? 
Well this is of course possible. I set out to find a program that I could use to do such a thing. 
Unfortunately I didn’t find any. So I decided to code my own that could brute force sub domains of a 
domain.
All I needed to do was code a program that pinged each sub domain to see if they were alive.
Although I am aware that it is of course possible that some sub domain’s will not reply to ICMP 
packets therefore making this piece of software I coded (DNS Brute) useless.
However it isn’t that likely that a group of servers are going to be set up that way.
For the purpose of this paper I will just run my program against Yahoo.com
This is the result:
www.SecurZone.Org                                       __________________  
As you can see we have found some useful information for Penetration Testing (no I am not 
conducting one against Yahoo, I am simply using this as an example.)
What we have found is the direct DNS’ of machines belonging to Yahoo.
*The source code to DNS Brute is included with this paper*
www.SecurZone.Org                                       __________________  
Performing A Ping Sweep
This is a really basic thing, which you should all know how to do. However since it is part of 
Information Gathering and being that there is a chance total beginners may read this, I will cover this.
The program we are going to use is Angry IP Scanner which can be downloaded from:
http://www.angryziber.com/ipscan/
There are 3 reasons why we are using Angry IP Scanner, the first being it is easy to use, the second 
being it is a very fast scanner and last of all it has some useful plug-ins for scanning.
Before we begin, for those of you who aren’t total beginners (and shouldn’t be reading this anyway) I
do realize what I am doing is actually more scanning than just doing a normal ping sweep, but why 
waste a ping sweep? Why not add in something interesting.
The first thing we have to do is get the IP Block/Range, this can usually be retrieved from the Whois, 
however if it isn’t simply estimate an approximate range based upon its IP Address.
Now we fire up Angry IP Scanner.
A neat thing about Angry IP Scanner is you can instantly convert a DNS to an IP Address (very 
handy.)
Now with this I have estimated an approximate IP range. Before I begin the scan, I will use one of 
Angry IP Scanners optional plug-ins which can be downloaded from the website.
If we go into Options -> Select Columns we can chose which plug-ins and information we want to 
view when doing a scan.
www.SecurZone.Org                                       __________________  
I have chosen to use all plug-ins that I think will show important information.
Now we begin the scan.
Ok now I haven’t completed the scan, but I have done enough. A useful thing about Angry IP Scanner 
is that you can cull all of all the dead hosts so you can have a good look at the live ones.
What have we been able to gather from this? Well first of all we have some host names.
The second thing I have learnt about Yahoo is they hide the identity of their web servers.
Now this Is a good security practice, Yahoo have been hacked before, and they don’t want website 
defacers scanning their IP range and looking for outdated web servers.
www.SecurZone.Org                                       __________________  
Identifying Firewalls/Routers
It is possible and quite a simple task to Identify remote routers, firewalls and other devices.
This can be done simply by using Trace Route.
Now both Windows and Linux operating systems have command line tools that allow us to 
accomplish this.
On Windows it is “tracert”, and on Linux “traceroute”.
Because I am using Windows at the moment I will demonstrate the use of tracert.
Tracing route to yahoo.com [66.94.234.13]
over a maximum of 30 hops:
  1    80 ms    69 ms    77 ms  10.0.0.2
  2    81 ms   100 ms   123 ms  ains-202-126-96-250.ains.net.au [202.126.96.250]
  3    71 ms    71 ms    71 ms  202.147.100.2
  4    74 ms    73 ms    77 ms  203.82.183.157
  5    75 ms    71 ms    75 ms  203.82.183.153
  6    74 ms    77 ms    73 ms  326.ge-0-0-0.GW4.MEL1.ALTER.NET [221.133.202.9]
  7    80 ms    75 ms    89 ms  424.AT-6-0-0.XR1.MEL1.ALTER.NET [210.80.33.161]
  8    82 ms    79 ms    81 ms  so-6-1-0.XR2.MEL1.ALTER.NET [210.80.33.26]
  9   106 ms   103 ms    97 ms  so-0-1-0.XT2.SYD4.ALTER.NET [210.80.33.9]
 10   182 ms    92 ms   125 ms  so-5-0-0.XT1.SYD4.ALTER.NET [210.80.33.213]
 11   249 ms   252 ms   249 ms  0.so-4-2-0.IR1.SAC2.Alter.Net [210.80.50.141]
 12   272 ms   243 ms   274 ms  POS3-0.IR1.SAC1.ALTER.NET [137.39.31.194]
 13   291 ms   337 ms   251 ms  0.so-0-0-0.TL1.SAC1.ALTER.NET [152.63.0.114]
 14   247 ms   247 ms   241 ms  0.so-5-0-0.XL1.SCL2.ALTER.NET [152.63.57.41]
 15   254 ms   253 ms   253 ms  0.so-6-0-0.BR1.SCL2.ALTER.NET [152.63.57.49]
 16   250 ms   249 ms   256 ms  204.255.173.42
 17   277 ms   273 ms   247 ms  so-1-2-0.bbr1.SanJose1.Level3.net [209.244.3.137]
 18   243 ms   247 ms   255 ms  ae-13-55.car3.SanJose1.Level3.net [4.68.123.141]
 19   248 ms   251 ms   249 ms  4.71.112.14
 20   282 ms   274 ms   253 ms  ge-3-0-0-p271.msr2.scd.yahoo.com [216.115.106.191]
 21   248 ms   269 ms   251 ms  ten-2-3-bas2.scd.yahoo.com [66.218.82.223]
 22   248 ms   267 ms   247 ms  w2.rc.vip.scd.yahoo.com [66.94.234.13]
Trace complete.
As you can see the first hop is my router, the second is my ISP.
At about the 19th hop the trace is approaching some of the Routers (or other devices) used by Yahoo.
In a penetration testing these devices are things you are going to want to focus on (among 
other things of course.)
For those of you who do not like using a Command Line there is a program available called Visual 
Route - http://www.visualroute.com/
Visual Route has a graphical user interface and actually has a world map you can visually see where 
each hop is on the trace.
www.SecurZone.Org                                       __________________  
Here is a screenshot of Visual Route:
One last thing about Trace Route.
You may notice when using the command line version that some hosts do not respond, yet the host 
afterwards does.
This means that the host that doesn’t respond isn’t responding to ICMP packets, yet allows ICMP 
packets to travel through. The command line version of Trace Route in both Windows and Linux uses
ICMP packets to do the trace.
www.SecurZone.Org                                       __________________  
The Linux equivalent of Tracert is Traceroute (exactly the same thing really.)
Usage: traceroute host.com
www.SecurZone.Org                                       __________________  
Gathering Information From Emails
Gathering Information from Emails you say? Yes, some very handy information can be gathered from 
emails.
You can obtain information simply by sending an email and then receiving one.
Useful information is contained in the email headers.
MIME-Version: 1.0
Received: from mail.networksolutionsemail.com ([205.178.146.55]) by bay0-mc3-
f10.bay0.hotmail.com with Microsoft SMTPSVC(6.0.3790.211); Fri, 30 Dec 2005 14:27:13 -0800
Received: (qmail 21220 invoked from network); 30 Dec 2005 22:26:56 -0000
Received: from unknown (HELO rsa33) (67.49.231.86) by 10.49.34.115 with SMTP; 30 Dec 2005 
22:26:56 -0000
X-Message-Info: JGTYoYF78jHECf9u/HR9ZmEHdSU/U1KwbJMtUcimNDw=
Organization: ksd,inc
X-MSMail-Priority: Normal
X-Mailer: Microsoft Outlook Express 6.00.2800.1437
X-MimeOLE: Produced By Microsoft MimeOLE V6.00.2800.1441
Return-Path: rsa@ksdinc.net
X-OriginalArrivalTime: 30 Dec 2005 22:27:13.0637 (UTC) FILETIME=[2EAFE950:01C60D90]
From this we can gather numerous pieces of information such as:
SMTP Server: mail.networksolutionsemail.com
SMTP Software: Microsoft SMTPSVC(6.0.3790.211)
Time/Date Email Received: 30 Dec 2005 22:26:56
IP Address Of Sender: 67.49.231.86
Email Software: Microsoft Outlook Express 6.00.2800.1437
From Email Address: rsa@ksdinc.net
So we have gathered some useful information. To get more information we can do a Whois on some 
of the IP Addresses and DNS’ above.
Have a look over the email headers and then have a look at the information I have listed out so you 
can see where I got it all from.
By default Hotmail (What Email provider I use) does not allow you to view all the headers of an email. 
To change this go to Options -> Mail Display Settings, then turn Message headers to Advanced.
Now for those of you who are using Microsoft Outlook this may help you:
http://office.microsoft.com/en-us/assistance/ha010937071033.aspx
www.SecurZone.Org                                       __________________  
Obtaining Information With Scripts
Using PHP and Javascript we can gather information on a target host. Now as most of you know this 
can only be done by getting them to view HTML (whether it be a website or HTML email.)
Here is a link which shows the sort of information which can be gathered using a script:
http://www.irongeek.com/browserinfo.php
Obviously we can’t personally use the link to gather information. However we can use other scripts to. 
I recommend:
http://statcounter.com
You can get a piece of Javascript code that when loaded on a remote machines can gather 
information on the person(s) loading the page.
I have one on my website http://911physics.co.nr
Although I mainly use Stat Counter to count how many unique visits my website gets I can also have 
a look at information relating to visitors that come to my website.
One thing I usually notice is that after a vulnerability is discovered in Firefox it usually takes 
people literally weeks to upgrade their browser! (To the latest version.)
Using Stat Counter is easy enough, it is just a matter of making an account and then getting the code 
to add into your webpage (or other HTML page.)
This is an example of the information that can be gathered:
We have successfully gather details on:
Operating System
Web Browser
Javascript Enabled/Disabled
As well as a couple of other potentially useful pieces of information.
www.SecurZone.Org                                       __________________  
About The Author
Aelphaeis Mangarae is an Administrator at Igniteds Security Forums – http://Igniteds.net as well as at 
Zone-H.org
I have authored several other White Papers which include Hardening Windows NT, XSS Attacks 
FAQ & Steganography FAQ.
I have also recently become an Administrator at SecurZone.Org.
(SecurZone.Org will be coming back soon!)
I am also a student member of the Scholars for 9/11 Truth – http://st911.org
IRC: irc.efnet.org #d-u
Email: adm1n1strat10n [AT] hotmail [DOT] com
MSN Messenger: adm1n1strat10n [AT] hotmail [DOT] com
Xbox Live Gamer Tag: Aelphaeis
Greetz To
htek, HackJoeSite, FRSilent, Read101, tomchu, nic`, BSoD, r0rkty, Nitrous, SyS64738, 
Trash-80, morning_wood, Astharot,  Fauley, Furax, PsAuX, SecurityWireless, SysSpider, 
Siegfried, fritz, darkt3ch,  Predator/ill skillz, Alchemist, BioHunter, Digerati, 
digital-flow, butthead, spiderlance, FishNET, W--, nrs, IBMWarpst, Nixus, varu, z16bitseg, 
PTP, felosi, Mega~biTe, wicked/aera, Palmeiro, Kadafiu, sNKenjoi, tgo, melkor, mu-tiger, royal, Wex, 
ksv, GoTiT4FrE, CKD, Dr4g, Coldfisher, snx, skiddieleet, ProwL, 
drygol, kon, Iadnah, EwenG, belgther, sarkar112, str0ke and Kenny & Blake from GSO.
sarkar112 – Thanks for pointing out that little grammar error in the draft paper.
htek,  tgo, wicked – Thanks for all the help with stuff unrelated to this paper. I really appreciate it.
r0rkty – Looking forward to running SecurZone with you! 
snx – Thanks for the SecurZone banner.
h4cky0u – How long are you going to ignore me for?
