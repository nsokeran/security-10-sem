import io

from src.algo_utils import get_best_sgd
from src.data_utils import get_texts_X_and_words, transform
from src.file_utils import read_all_converted_texts, read_pdf_io
from src.ml_utils import trim_absent_classes, get_classes, get_names_by_class_ids

from flask import Flask
from flask import request

from src.nl_utils import extract_stemmed_text

texts = read_all_converted_texts("./converted")
text_names = [text[0] for text in texts]
labels_X, texts_X = get_texts_X_and_words(list(map(lambda x: x[1], texts)))
binary_y = get_classes(text_names)

new_X, new_y = trim_absent_classes(texts_X, binary_y)
sgd_gs, sgd = get_best_sgd(new_X, new_y)

app = Flask(__name__)


@app.route('/', methods=['POST'])
def parse_request():
    file = request.files['file']
    tempfile = file.stream
    tempfile.rollover()
    bytes_io = io.BytesIO(tempfile.read())
    raw_text = read_pdf_io(bytes_io)
    stem_text = extract_stemmed_text(raw_text)
    x = transform([stem_text]).reshape(1, -1)
    y = sgd.predict(x)[0]
    tags = get_names_by_class_ids(y)
    return str(tags)
